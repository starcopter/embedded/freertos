# FreeRTOS

This repository serves as a thin wrapper around the [upstream FreeRTOS Kernel][kernel] on GitHub.

A few links:

- [FreeRTOS homepage][]
- [Developer Docs][]
- [Quick Start Guide][]
- [`FreeRTOSConfig.h` Reference][]

## Integration Guide

This is a short summary to help integrating FreeRTOS into a project.
It covers adding the necessary sources into the git tree, setting up the build system and IDE (VSCode),
synchronizing CubeMX and FreeRTOS configuration and starting the RTOS scheduler at the right location.

Most of the steps are taken from FreeRTOS' [integration guide][Integration], with some experience from the [GVR implementation][] mixed in.
This overview should _not_ replace FreeRTOS' [Developer Docs][] or [Quick Start Guide][] :wink:

### Makefile

Some changes need to be made in each project's Makefile, to configure the build system how to integrate FreeRTOS' headers and sources.

The FreeRTOS core is written in a platform and compiler independent way.
To facitlitate the small differences between different setups, all implementation-dependent code is located in a `port.c` and a `portmacro.h`,
residing in `kernel/portable/<compiler>/<architecture>/`.
The STM32G4 series processors run on an `ARM_CM4F` (or `ARM_CM4_MPU` with the memory protection unit active), the STM32H7 series on an `ARM_CM7/r0p1` architecture.
This port information needs to be added to the Makefile as the `FREERTOS_PORT` variable.

```make
# FreeRTOS port selection
FREERTOS_PORT = ARM_CM4F
```

Next, `make` needs to be told where the FreeRTOS base directory resides in the project's folder structure.
_The base directory is the directory this `README.md` is located in._

```make
# FreeRTOS base directory
FREERTOS_DIR = lib/rtos
```

Finally the FreeRTOS source and header files need to be added to the Makefile's `C_SOURCES` and `C_INCLUDES`, respectively.

```make
# C sources
C_SOURCES =  \
# [...]
$(wildcard $(FREERTOS_DIR)/kernel/*.c) \
$(FREERTOS_DIR)/kernel/portable/GCC/$(FREERTOS_PORT)/port.c \
$(FREERTOS_DIR)/kernel/portable/MemMang/heap_4.c \
# [...]

# C includes
C_INCLUDES =  \
# [...]
-I$(FREERTOS_DIR)/kernel/include \
-I$(FREERTOS_DIR)/kernel/portable/GCC/$(FREERTOS_PORT) \
# [...]
```

### Timebase: FreeRTOS and ST's HAL

As described in [this ST community forum post][forum post], FreeRTOS likes keeping the Cortex System Timer (SysTick)
to itself. To decouple ST's HAL from the (FreeRTOS-managed) System Timer, configure a different and
otherwise unused timer (e.g. TIM6 or TIM7) as HAL timebase:

![CubeMX Timebase Selection](.img/timebase.png)

  [forum post]: https://community.st.com/s/question/0D50X0000A4nQxpSQE/when-freertos-is-used-it-is-strongly-recommended-to-use-a-hal-time-base-source-other-than-the-systick-

### Interrupt Priorities

_**Note**: in Cortex-M, a higher interrupt priority is indicated by a lower numeric value!_

FreeRTOS' scheduler utilizes (at least on Cortex-M4 architecture) the above mentioned SysTick timer and the SVCall and
PendSV interrupts, therefore it operates from _inside interrupt service routines_.
Since the scheduler should generally stay out of the way of all "real" interrupts, it is advised to configure the
scheduler-used interrupts to run at the lowest possible priority, set by the configuration macro
`configKERNEL_INTERRUPT_PRIORITY`.

Some kernel internals require the _maximum interrupt priority that can still talk to FreeRTOS_ to be defined,
which is configured by the `configMAX_SYSCALL_INTERRUPT_PRIORITY` macro.
All interrupt handlers that need to interface with FreeRTOS (e.g. using [Queues][xQueueSendToBackFromISR],
[Task Notifications][vTaskNotifyGiveFromISR], [Semaphores][xSemaphoreGiveFromISR] or
[Message Buffers][xMessageBufferSendFromISR]) will need to run at a priority **at or below** the configured
`configMAX_SYSCALL_INTERRUPT_PRIORITY`. As an example, with a `configMAX_SYSCALL_INTERRUPT_PRIORITY` of 5,
a numerical preemption priority at or above 5 would be acceptable.

FreeRTOS automatically reconfigures the SysTick and PendSV interrupt priorities to `configKERNEL_INTERRUPT_PRIORITY`,
therefore the configuration for these two interrupts in CubeMX is irrelevant and will be overridden once the scheduler
is started.

All four priority bits should be used for preemption priority, and none for subpriority.

![CubeMX NVIC Priorities](.img/nvic-priorities.png)

FreeRTOS brings its own IRQ handler implementation for the SysTick, PendSV and SVCall interrupts,
therefore the code generation for these three handlers should be disabled in CubeMX:

![CubeMX NVIC code Generation](.img/nvic-code-generation.png)

  [xQueueSendToBackFromISR]: https://www.freertos.org/xQueueSendToBackFromISR.html
  [vTaskNotifyGiveFromISR]: https://www.freertos.org/vTaskNotifyGiveFromISR.html
  [xSemaphoreGiveFromISR]: https://www.freertos.org/a00124.html
  [xMessageBufferSendFromISR]: https://www.freertos.org/xMessageBufferSendFromISR.html

### FreeRTOS Configuration File

FreeRTOS is configured by the `FreeRTOSConfig.h` file, which needs to be located anywhere in the include path.
Most configuration options are pretty straight forward and well documented in the
[online Reference][`FreeRTOSConfig.h` Reference].

An example configuration file is provided with [FreeRTOSConfig-EXAMPLE.h](FreeRTOSConfig-EXAMPLE.h) in this repository.

A few notable platform- or architecture-specific settings include:

#### Referencing the `SystemCoreClock`

CubeMX exports the (initially configured) system core frequency to the `SystemCoreClock` variable.
By declaring the symbol in the configuration header, we can tie FreeRTOS directly to this value:

```c
/* Ensure stdint is only used by the compiler, and not the assembler. */
#if defined(__ICCARM__) || defined(__CC_ARM) || defined(__GNUC__)
    #include <stdint.h>
    extern uint32_t SystemCoreClock;
#endif
```

#### SysTick Frequency

At least the [STM32G4 Series][G4 Series] clock the 24-Bit SystemTick Timer with a frequency different from the Core.
As stated in the Reference Manual (RM0440, Reset and Clock Control (RCC), page 277:)
> The RCC feeds the Cortex® System Timer (SysTick) external clock with the AHB clock (HCLK) divided by 8.

```c
#define configCPU_CLOCK_HZ                      ( SystemCoreClock )
#define configSYSTICK_CLOCK_HZ                  ( ( SystemCoreClock ) / 8 )
```

#### `configPRIO_BITS`

The number of preemption priority bits is exported by CubeMX to the CMSIS-defined `__NVIC_PRIO_BITS`
and can be read from there:

```c
#ifdef __NVIC_PRIO_BITS
    #define configPRIO_BITS                     __NVIC_PRIO_BITS
#else
    #define configPRIO_BITS                     4  /* 15 priority levels */
#endif
```

#### Interrupt Priority Configuration

As discussed [above](#interrupt-priorities), the FreeRTOS-relevant interrupt priorities need to be in sync with the
CubeMX configuration.

```c
#define configKERNEL_INTERRUPT_PRIORITY         ( (uint8_t) (0xff << ( 8 - configPRIO_BITS )) )
#define configMAX_SYSCALL_INTERRUPT_PRIORITY    ( 5 << ( 8 - configPRIO_BITS ) )
```

#### Interrupt Handlers

CubeMX creates a CMSIS compliant vector table (located in `startup_stm32*.s` in the project directory), populated with
the CMSIS defined IRQ handler names. To make the table point to the FreeRTOS-provided handler implementations
_without having to mess with the vector table itself_, we can remap FreeRTOS' function names to their CMSIS-compliant
counterparts:

```c
#define vPortSVCHandler     SVC_Handler
#define xPortPendSVHandler  PendSV_Handler
#define xPortSysTickHandler SysTick_Handler
```

#### `configASSERT()`

As documented in the [reference][`FreeRTOSConfig.h` Reference]:
> configASSERT() is called throughout the FreeRTOS source files to check how the application is using FreeRTOS.
> It is highly recommended to develop FreeRTOS applications with configASSERT() defined.

The referenced function `vAssertCalled` does not exist and only serves as a placeholder.
It could be replaced by a call to the ST implementation `void assert_failed(uint8_t *file, uint32_t line)`.

```c
#define configASSERT( x ) if( ( x ) == 0 ) vAssertCalled( __FILE__, __LINE__ )
```

  [G4 Series]: https://www.st.com/en/microcontrollers-microprocessors/stm32g4-series.html
  [FreeRTOS homepage]: https://www.freertos.org/index.html
  [kernel]: https://github.com/FreeRTOS/FreeRTOS-Kernel/
  [Developer Docs]: https://www.freertos.org/features.html
  [Quick Start Guide]: https://www.freertos.org/FreeRTOS-quick-start-guide.html
  [`FreeRTOSConfig.h` Reference]: https://www.freertos.org/a00110.html
  [Integration]: https://www.freertos.org/Creating-a-new-FreeRTOS-project.html
  [GVR implementation]: https://gitlab.com/gvr-clausthal/gvr-common/-/tree/develop/FreeRTOS